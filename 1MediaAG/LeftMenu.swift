//
//  LeftMenu.swift
//  1MediaAG
//
//  Created by Chintan on 02/05/16.
//  Copyright © 2016 solulab. All rights reserved.
//

import UIKit
protocol sideDelegate
{
    func pushToViewController(controller : UIViewController)
}

class LeftMenu: UIViewController {

    @IBOutlet var tblMenuItem: UITableView!
    var mainVC : MainView!
    var viewController : ViewController!

    var delegatePush : sideDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    // MARK: - Tableview Delegate DataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 2
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 50
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("LeftViewCell") as! LeftViewCell
     
        if(indexPath.row == 0){
            cell.lblTitle.text = FirstScreen_Header
        }else if(indexPath.row == 1){
            cell.lblTitle.text = SecondScreen_Header
        }
        cell.selectionStyle = .None
        return cell
    }
    
    func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath) {
        let cell  = tableView.cellForRowAtIndexPath(indexPath)
        cell!.contentView.backgroundColor = .clearColor()
    }
    
    func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
        let cell  = tableView.cellForRowAtIndexPath(indexPath)
        cell!.contentView.backgroundColor = .clearColor()
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        tblMenuItem.deselectRowAtIndexPath(indexPath, animated: true)
        
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            mainVC = storyBoard.instantiateViewControllerWithIdentifier("MainView") as! MainView
            if(indexPath.row == 0){
                mainVC.strTitle = FirstScreen_Header
            }else{
                mainVC.strTitle = SecondScreen_Header
            }
            self.delegatePush.pushToViewController(mainVC)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
