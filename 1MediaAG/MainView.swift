//
//  MainView.swift
//  1MediaAG
//
//  Created by Chintan on 02/05/16.
//  Copyright © 2016 solulab. All rights reserved.
//

import UIKit

class MainView: UIViewController, sideDelegate {

    @IBOutlet var lblTitleScreen: UILabel!
    @IBOutlet var btnMenu: UIButton!
    var swrevalVC : SWRevealViewController!
    var strTitle : String! = FirstScreen_Header
    @IBOutlet var lblMidTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        swrevalVC = self.revealViewController()
        swrevalVC.panGestureRecognizer()
        swrevalVC.tapGestureRecognizer()
        self.revealViewController().panGestureRecognizer().enabled = true

        appDelegate.leftController.delegatePush = self
    }
    
    override func viewWillAppear(animated: Bool) {
        lblTitleScreen.text = strTitle
        if(strTitle.containsString("Second")){
            lblMidTitle.text = SecondScreen_Description
        }else{
            lblMidTitle.text = FirstScreen_Description
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func leftbtnPress(sender: UIButton) {
         swrevalVC.revealToggle(sender)
    }
    @IBAction func btnRight(sender: UIButton) {
        swrevalVC.rightRevealToggle(sender)
    }
    // MARK: - Delegate Method
    
    func pushToViewController(controller: UIViewController) {
        swrevalVC.revealToggle(btnMenu)
        self.navigationController?.pushViewController(controller, animated: false)
    }

  
}
