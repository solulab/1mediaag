//
//  ViewController.swift
//  1MediaAG
//
//  Created by Chintan on 02/05/16.
//  Copyright © 2016 solulab. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate, sideDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var textView: UITextView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var animatedImage: UIImageView!
    @IBOutlet var backgroundImageFirstScreen: UIImageView!
    @IBOutlet var backgroundImageSecondScreen: UIImageView!
    @IBOutlet var backgroundImageThirdScreen: UIImageView!
    @IBOutlet var backgroundImageForthScreen: UIImageView!
    @IBOutlet var backgroundImageFifthScreen: UIImageView!

    var lblFirstScreenSubtitle : UILabel!
    var lblSecondScreenSubtitle : UILabel!
    var lblThirdScreenSubtitle : UILabel!
     
     
     
    var lblFourthScreenSubtitle : UILabel!
    var lblFifthScreenSubtitle : UILabel!
    var lblFirstScreenTitle : UILabel!
    var lblSecondScreenTitle : UILabel!
    var lblThirdScreenTitle : UILabel!
    var lblForthScreenTitle : UILabel!
     
     
     
    var lblFifthScreenTitle : UILabel!
    var yourCaseLogo : UIImageView!
    
    var fontName : String = "SFUIDisplay-Regular"
    
    //Variables below used to store height and y position title and subtitle to perform animation
    var yPositionForTitleLabel : CGFloat! = 340
    var yPositionForSubtitleLabel  : CGFloat! = 368
    var heightForTitleLabel : CGFloat! = 30
    var heightForSubtitleLabel : CGFloat! = 100
    
    
    @IBOutlet var startButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        appDelegate.leftController.delegatePush = self
        
        //Set scroll view
        self.scrollView.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
        //Set Title
            self.setTitleForAllScreens()
        //Set Subtitle
            self.setSubtitleForAllScreens()
        //Set content size of Scroll view
        self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.width * 5, self.scrollView.frame.height)
        self.scrollView.delegate = self
        self.pageControl.currentPage = 0
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Method used for image animation and color trasform
    //The logic is to complete the animation for left/right screen when user scrolls the scroll bar to 30%. The same calculation applies in reverse direction as well. As this was one of animation, we haven't used any library/framework0p----
    func setScrollViewAnimation(scrollViewObj:UIScrollView, width:CGFloat, firstTitle: UILabel, secondTitle: UILabel, firstSubtitle: UILabel, secondSubtitle: UILabel, firstImage:UIImageView, secondImage:UIImageView) {
        
        let scrollViewWidth = scrollViewObj.frame.size.width
        let scrollContentOffset = scrollView.contentOffset.x - width
        
        let percentForImageTransform = (scrollContentOffset / (scrollViewWidth/3))
        let ReversePercentForImageTransform = 1 - percentForImageTransform
        let percentForLabelTransform = (scrollContentOffset / scrollViewWidth)
        let ReversePercentForLabelTransform = 1 - percentForLabelTransform
        
        firstSubtitle.alpha = ReversePercentForLabelTransform
        firstTitle.alpha = ReversePercentForLabelTransform
        secondSubtitle.alpha = percentForLabelTransform
        secondTitle.alpha = percentForLabelTransform
        
        if(width == 0){
            yourCaseLogo.alpha = ReversePercentForLabelTransform
        } else {
            yourCaseLogo.alpha = 0
        }
        firstImage.alpha = ReversePercentForLabelTransform
        secondImage.alpha = percentForLabelTransform
        if(percentForImageTransform > 2.0)
        {
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.animatedImage.transform = CGAffineTransformScale(CGAffineTransformIdentity, percentForImageTransform-2.0, percentForImageTransform-2.0)
                self.animatedImage.alpha = percentForImageTransform-2.0
            })
        }
        else if(percentForImageTransform < 1.0)
        {
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.animatedImage.transform = CGAffineTransformScale(CGAffineTransformIdentity, ReversePercentForImageTransform, ReversePercentForImageTransform)
                self.animatedImage.alpha = ReversePercentForImageTransform
            })
        }
    }
    
    //MARK: UIScrollViewDelegate
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        let scrollViewWidth:CGFloat = self.scrollView.frame.width
        
        if(scrollView.contentOffset.x > 0 && scrollView.contentOffset.x <= scrollViewWidth) {
            
            self.setScrollViewAnimation(self.scrollView, width: 0, firstTitle: lblFirstScreenTitle, secondTitle: lblSecondScreenTitle, firstSubtitle: lblFirstScreenSubtitle, secondSubtitle: lblSecondScreenSubtitle,firstImage: backgroundImageSecondScreen,secondImage: backgroundImageFirstScreen)
            
        } else if(scrollView.contentOffset.x > scrollViewWidth && scrollView.contentOffset.x <= scrollViewWidth*2){
            
            self.setScrollViewAnimation(self.scrollView, width: scrollViewWidth, firstTitle: lblSecondScreenTitle, secondTitle: lblThirdScreenTitle, firstSubtitle: lblSecondScreenSubtitle, secondSubtitle: lblThirdScreenSubtitle,firstImage: backgroundImageFirstScreen,secondImage: backgroundImageThirdScreen)
            
        } else if(scrollView.contentOffset.x > scrollViewWidth*2 && scrollView.contentOffset.x <= scrollViewWidth*3){
            
            self.setScrollViewAnimation(self.scrollView, width: scrollViewWidth*2, firstTitle: lblThirdScreenTitle, secondTitle: lblForthScreenTitle, firstSubtitle: lblThirdScreenSubtitle, secondSubtitle: lblFourthScreenSubtitle,firstImage: backgroundImageThirdScreen,secondImage: backgroundImageForthScreen)
            
        } else if(scrollView.contentOffset.x > scrollViewWidth*3 && scrollView.contentOffset.x <= scrollViewWidth*4){
            
            self.setScrollViewAnimation(self.scrollView, width: scrollViewWidth*3, firstTitle: lblForthScreenTitle, secondTitle: lblFifthScreenTitle, firstSubtitle: lblFourthScreenSubtitle, secondSubtitle: lblFifthScreenSubtitle,firstImage: backgroundImageForthScreen,secondImage: backgroundImageFifthScreen)
        }
    }
    
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView){
        // Set current screen/page index
        let pageWidth:CGFloat = CGRectGetWidth(scrollView.frame)
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        self.pageControl.currentPage = Int(currentPage);
    }
    
    // MARK: - Delegate Method
    
    func pushToViewController(controller: UIViewController) {
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    //Set title text + position for all screen as provided in Sketch
    func setTitleForAllScreens()
    {
        let scrollViewWidth:CGFloat = self.scrollView.frame.width

        lblFirstScreenTitle = UILabel()
        lblFirstScreenTitle.frame = CGRectMake(60, yPositionForTitleLabel, scrollViewWidth-120, 60)
        lblFirstScreenTitle.text = Onboard_FirstScreen_Title
        lblFirstScreenTitle.textAlignment = .Center
        lblFirstScreenTitle.font = UIFont(name: fontName, size: 24)
        lblFirstScreenTitle.textColor = UIColor.whiteColor()
        lblFirstScreenTitle.numberOfLines = 2
        
        yourCaseLogo = UIImageView()
        yourCaseLogo.frame = CGRectMake(108, 34, 150, 50)
        yourCaseLogo.image = UIImage(named: "Onlogo")

        lblSecondScreenTitle = UILabel()
        lblSecondScreenTitle.frame = CGRectMake(scrollViewWidth+60, yPositionForTitleLabel, scrollViewWidth-120, heightForTitleLabel)
        lblSecondScreenTitle.text = Onboard_SecondScreen_Title
        lblSecondScreenTitle.textAlignment = .Center
        lblSecondScreenTitle.font = UIFont(name: fontName, size: 24)
        lblSecondScreenTitle.textColor = UIColor.whiteColor()
        lblSecondScreenTitle.numberOfLines = 2
        
        lblThirdScreenTitle = UILabel()
        lblThirdScreenTitle.frame = CGRectMake((scrollViewWidth*2)+60, yPositionForTitleLabel, scrollViewWidth-120, heightForTitleLabel)
        lblThirdScreenTitle.text = Onboard_ThirdScreen_Title
        lblThirdScreenTitle.textAlignment = .Center
        lblThirdScreenTitle.font = UIFont(name: fontName, size: 24)
        lblThirdScreenTitle.textColor = UIColor.whiteColor()
        lblThirdScreenTitle.numberOfLines = 2
        
        lblForthScreenTitle = UILabel()
        lblForthScreenTitle.frame = CGRectMake((scrollViewWidth*3)+60, yPositionForTitleLabel, scrollViewWidth-120, heightForTitleLabel)
        lblForthScreenTitle.text = Onboard_ForthScreen_Title
        lblForthScreenTitle.textAlignment = .Center
        lblForthScreenTitle.font = UIFont(name: fontName, size: 24)
        lblForthScreenTitle.textColor = UIColor.whiteColor()
        lblForthScreenTitle.numberOfLines = 2
        
        lblFifthScreenTitle = UILabel()
        lblFifthScreenTitle.frame = CGRectMake((scrollViewWidth*4)+50, yPositionForTitleLabel, scrollViewWidth-100, 60)
        lblFifthScreenTitle.text = Onboard_FifthScreen_Title
        lblFifthScreenTitle.textAlignment = .Center
        lblFifthScreenTitle.font = UIFont(name: fontName, size: 24)
        lblFifthScreenTitle.textColor = UIColor.whiteColor()
        lblFifthScreenTitle.numberOfLines = 2
        
        self.scrollView.addSubview(lblFirstScreenTitle)
        self.scrollView.addSubview(lblSecondScreenTitle)
        self.scrollView.addSubview(lblThirdScreenTitle)
        self.scrollView.addSubview(lblForthScreenTitle)
        self.scrollView.addSubview(lblFifthScreenTitle)
        self.scrollView.addSubview(yourCaseLogo)
    }
    
    //Set subtitle text + position for all screen as provided in Sketch
    func setSubtitleForAllScreens()
    {
        let scrollViewWidth:CGFloat = self.scrollView.frame.width

        lblFirstScreenSubtitle = UILabel()
        lblFirstScreenSubtitle.frame = CGRectMake(65, yPositionForSubtitleLabel+30, scrollViewWidth-130, heightForSubtitleLabel)
        lblFirstScreenSubtitle.text = Onboard_Subtitle
        lblFirstScreenSubtitle.textAlignment = .Center
        lblFirstScreenSubtitle.font = UIFont.systemFontOfSize(15)
        lblFirstScreenSubtitle.textColor = UIColor.whiteColor()
        lblFirstScreenSubtitle.numberOfLines = 0
        
        lblSecondScreenSubtitle = UILabel()
        lblSecondScreenSubtitle.frame = CGRectMake(scrollViewWidth+65, yPositionForSubtitleLabel, scrollViewWidth-130, heightForSubtitleLabel)
        lblSecondScreenSubtitle.text = Onboard_Subtitle
        lblSecondScreenSubtitle.textAlignment = .Center
        lblSecondScreenSubtitle.font = UIFont.systemFontOfSize(15)
        lblSecondScreenSubtitle.textColor = UIColor.whiteColor()
        lblSecondScreenSubtitle.numberOfLines = 0
        
        lblThirdScreenSubtitle = UILabel()
        lblThirdScreenSubtitle.frame = CGRectMake((scrollViewWidth*2)+65, yPositionForSubtitleLabel, scrollViewWidth-130, heightForSubtitleLabel)
        lblThirdScreenSubtitle.text = Onboard_Subtitle
        lblThirdScreenSubtitle.textAlignment = .Center
        lblThirdScreenSubtitle.font = UIFont.systemFontOfSize(15)
        lblThirdScreenSubtitle.textColor = UIColor.whiteColor()
        lblThirdScreenSubtitle.numberOfLines = 0
        
        lblFourthScreenSubtitle = UILabel()
        lblFourthScreenSubtitle.frame = CGRectMake((scrollViewWidth*3)+65, yPositionForSubtitleLabel, scrollViewWidth-130, heightForSubtitleLabel)
        lblFourthScreenSubtitle.text = Onboard_Subtitle
        lblFourthScreenSubtitle.textAlignment = .Center
        lblFourthScreenSubtitle.font = UIFont.systemFontOfSize(15)
        lblFourthScreenSubtitle.textColor = UIColor.whiteColor()
        lblFourthScreenSubtitle.numberOfLines = 0
        
        lblFifthScreenSubtitle = UILabel()
        lblFifthScreenSubtitle.frame = CGRectMake((scrollViewWidth*4)+65, yPositionForSubtitleLabel+30, scrollViewWidth-130, heightForSubtitleLabel)
        lblFifthScreenSubtitle.text = Onboard_Subtitle
        lblFifthScreenSubtitle.textAlignment = .Center
        lblFifthScreenSubtitle.font = UIFont.systemFontOfSize(15)
        lblFifthScreenSubtitle.textColor = UIColor.whiteColor()
        lblFifthScreenSubtitle.numberOfLines = 0
        
        self.scrollView.addSubview(lblFirstScreenSubtitle)
        self.scrollView.addSubview(lblSecondScreenSubtitle)
        self.scrollView.addSubview(lblThirdScreenSubtitle)
        self.scrollView.addSubview(lblFourthScreenSubtitle)
        self.scrollView.addSubview(lblFifthScreenSubtitle)
    }
    
}


