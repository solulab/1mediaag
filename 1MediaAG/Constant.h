//
//  Constant.h
//  1MediaAG
//
//  Created by Chintan on 04/05/16.
//  Copyright © 2016 solulab. All rights reserved.
//

#ifndef Constant_h
#define Constant_h

#define Onboard_FirstScreen_Title  @"Gestalte deine Hülle mit deinen Fotos!"
#define Onboard_SecondScreen_Title  @"Wähle dein Gerät"
#define Onboard_ThirdScreen_Title  @"Lade dein Foto hoch"
#define Onboard_ForthScreen_Title  @"Gestalte deine Hülle"
#define Onboard_FifthScreen_Title  @"In der App bestellen mit kostenlosem Versand"

#define Onboard_Subtitle  @"Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor Lorem ipsum"

#define SecondScreen_Description @"This is the second content screen"
#define FirstScreen_Description @"This is the first content screen"
#define FirstScreen_Header "First content screen"
#define SecondScreen_Header "Second content screen"
#define Title_Font      @"SFUIText-Regular"


#endif /* Constant_h */
